package com.np.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping(value="/hello", method = RequestMethod.GET)
	public String sayhi(){
		System.out.println("Home Page");
		return "hiii";
	}
	
	@RequestMapping("/")
	public String allDefault(){
		System.out.println("Invalid Url");
		return "Invalid URL";
	}
}
